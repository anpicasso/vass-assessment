package com.vass.assessment;

import com.vass.assessment.controller.ClienteController;
import com.vass.assessment.controller.ProductoController;
import com.vass.assessment.controller.ReclamoController;
import com.vass.assessment.dao.ClienteDao;
import com.vass.assessment.dao.ProductoDao;
import com.vass.assessment.dao.ReclamoDao;
import com.vass.assessment.repository.ClienteRepository;
import com.vass.assessment.repository.ProductoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ExtendWith(MockitoExtension.class)
public class AbstractTest {

    private static EntityManager entityManager;
    private static ModelMapper modelMapper;

    private static ProductoRepository productoRepository;
    private static ClienteRepository clienteRepository;
    private static ReclamoRepository reclamoRepository;

    protected static ClienteController clienteController;
    protected static ProductoController productoController;
    protected static ReclamoController reclamoController;

    private static Answer retMismoObjeto = invocation -> invocation.getArguments()[0]; //retornar mismo objecto

    @BeforeAll
    public static void setUpOnce() {
        entityManager = Mockito.mock(EntityManager.class);
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        clienteRepository = Mockito.mock(ClienteRepository.class);
        productoRepository = Mockito.mock(ProductoRepository.class);
        reclamoRepository = Mockito.mock(ReclamoRepository.class);

        Mockito.when(clienteRepository.save(ArgumentMatchers.any())).thenAnswer(retMismoObjeto);
        Mockito.when(productoRepository.save(ArgumentMatchers.any())).thenAnswer(retMismoObjeto);
        Mockito.when(reclamoRepository.save(ArgumentMatchers.any())).thenAnswer(retMismoObjeto);

        ClienteDao clienteDao = new ClienteDao(modelMapper, productoRepository, clienteRepository, reclamoRepository);
        ProductoDao productoDao = new ProductoDao(modelMapper, productoRepository, clienteRepository, reclamoRepository);
        ReclamoDao reclamoDao = new ReclamoDao(modelMapper, productoRepository, clienteRepository, reclamoRepository);

        clienteDao.setEntityManager(entityManager);
        productoDao.setEntityManager(entityManager);
        reclamoDao.setEntityManager(entityManager);

        clienteController = new ClienteController(clienteDao);
        productoController = new ProductoController(productoDao);
        reclamoController = new ReclamoController(reclamoDao);
    }
}
