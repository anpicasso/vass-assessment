package com.vass.assessment.controller;

import com.vass.assessment.AbstractTest;
import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ProductoControllerTest extends AbstractTest {

    @Test
    void registrarProducto() {
        ProductoDto productoDto = new ProductoDto();
        productoDto.setNombreProducto("PRODUCTO");
        assertEquals(productoDto.getCodigoCliente(),productoController.registrarProducto(productoDto).getEntity().getCodigoProducto());
    }

}