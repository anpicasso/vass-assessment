package com.vass.assessment.controller;

import com.vass.assessment.AbstractTest;
import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.model.Cliente;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class ClienteControllerTest extends AbstractTest {

    @Test
    void registrarCliente() {
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setNumeroDocumento("77777777");
        assertEquals(clienteDto.getCodigoCliente(),clienteController.registrarCliente(clienteDto).getEntity().getCodigoCliente());
    }
}