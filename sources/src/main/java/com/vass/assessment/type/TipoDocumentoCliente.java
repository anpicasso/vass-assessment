package com.vass.assessment.type;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum TipoDocumentoCliente {
    DNI,
    PASAPORTE,
    CARNET_EXTRANJERIA
}
