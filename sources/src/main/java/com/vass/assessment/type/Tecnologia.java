package com.vass.assessment.type;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum Tecnologia {
    JAVA,
    CPLUS,
    CSHARP
}
