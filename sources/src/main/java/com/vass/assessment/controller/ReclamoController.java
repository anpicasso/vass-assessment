package com.vass.assessment.controller;

import com.vass.assessment.dao.ProductoDao;
import com.vass.assessment.dao.ReclamoDao;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.ReclamoDto;
import com.vass.assessment.dto.response.Response;
import com.vass.assessment.repository.ReclamoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reclamo")
public class ReclamoController {

    private ReclamoDao reclamoDao;

    public ReclamoController(ReclamoDao reclamoDao) {
        this.reclamoDao = reclamoDao;
    }

    @GetMapping(path="/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ReclamoDto> listarReclamo() {
        return reclamoDao.listarReclamo();
    }

    @GetMapping(path="/buscar/{codigoReclamo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ReclamoDto> buscarReclamo(@PathVariable String codigoReclamo) {
        return reclamoDao.buscarReclamo(codigoReclamo);
    }

    @GetMapping(path="/buscarxcliente/{codigoCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ReclamoDto> buscarReclamosPorCodigoCliente(@PathVariable String codigoCliente) {
        return reclamoDao.buscarReclamosPorCodigoCliente(codigoCliente);
    }

    @GetMapping(path="/buscarxproducto/{codigoProducto}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ReclamoDto> buscarReclamosPorCodigoProducto(@PathVariable String codigoProducto) {
        return reclamoDao.buscarReclamosPorCodigoProducto(codigoProducto);
    }

    @PostMapping(path = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ReclamoDto> registrarReclamo(@RequestBody ReclamoDto reclamoDto) {
        return reclamoDao.registrarReclamo(reclamoDto);
    }

}
