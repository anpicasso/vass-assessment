package com.vass.assessment.controller;

import com.vass.assessment.dao.ClienteDao;
import com.vass.assessment.dao.ProductoDao;
import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/producto")
public class ProductoController {

    private ProductoDao productoDao;

    public ProductoController(ProductoDao productoDao) {
        this.productoDao = productoDao;
    }

    @PostMapping(path = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ProductoDto> registrarProducto(@RequestBody ProductoDto clienteDto) {
        return productoDao.registrarProducto(clienteDto);
    }

    @GetMapping(path="/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ProductoDto> listarProducto() {
        return productoDao.listarProducto();
    }

    @GetMapping(path="/buscar/{codigoProducto}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ProductoDto> buscarProducto(@PathVariable String codigoProducto) {
        return productoDao.buscarProducto(codigoProducto);
    }

    @GetMapping(path="/buscarxcliente/{codigoCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ProductoDto> buscarProductosPorCodigoCliente(@PathVariable String codigoCliente) {
        return productoDao.buscarProductosPorCodigoCliente(codigoCliente);
    }

}
