package com.vass.assessment.controller;

import com.vass.assessment.dao.ClienteDao;
import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/cliente")
public class ClienteController {

    private ClienteDao clienteDao;

    public ClienteController(ClienteDao clienteDao) {
        this.clienteDao = clienteDao;
    }

    @PostMapping(path = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ClienteDto> registrarCliente(@RequestBody ClienteDto clienteDto) {
        return clienteDao.registrarCliente(clienteDto);
    }

    @PostMapping(path ="/contratar-producto", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ClienteDto> contratarProducto(@RequestBody ProductoDto productoDto) {
        return clienteDao.contratarProducto(productoDto);
    }

    @GetMapping(path="/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ClienteDto> listarCliente() {
        return clienteDao.listarCliente();
    }

    @GetMapping(path="/buscar/{codigoCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<ClienteDto> buscarCliente(@PathVariable String codigoCliente) {
        return clienteDao.buscarCliente(codigoCliente);
    }
}
