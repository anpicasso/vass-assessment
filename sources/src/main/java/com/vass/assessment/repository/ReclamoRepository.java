package com.vass.assessment.repository;

import com.vass.assessment.model.Reclamo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReclamoRepository extends JpaRepository<Reclamo, Long> {

    Optional<Reclamo> findByCodigoReclamo(String codigoReclamo);
    List<Reclamo> findByProductoAsociado_CodigoProducto(String codigoProducto);

}
