package com.vass.assessment.dao;

import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.ReclamoDto;
import com.vass.assessment.dto.ReclamoDto;
import com.vass.assessment.dto.response.Response;
import com.vass.assessment.model.Cliente;
import com.vass.assessment.model.Producto;
import com.vass.assessment.model.Reclamo;
import com.vass.assessment.model.Reclamo;
import com.vass.assessment.repository.ClienteRepository;
import com.vass.assessment.repository.ProductoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ReclamoDao extends AbstractDao<Reclamo, ReclamoDto> {

    @Autowired
    public ReclamoDao(ModelMapper modelMapper, ProductoRepository productoRepository, ClienteRepository clienteRepository, ReclamoRepository reclamoRepository) {
        super(modelMapper, productoRepository, clienteRepository, reclamoRepository);
    }

    public Response<ReclamoDto> listarReclamo() {
        var reclamos = mapListaEntidades(reclamoRepository.findAll(), ReclamoDto.class);
        return new Response<ReclamoDto>().ok().data(reclamos);
    }

    public Response<ReclamoDto> buscarReclamo(String codigoReclamo) {
        var reclamo = reclamoRepository.findByCodigoReclamo(codigoReclamo);
        if(reclamo.isEmpty()) {
            return new Response<ReclamoDto>().error();
        }
        return new Response<ReclamoDto>().ok().entity(mapEntidad(reclamo.get(), ReclamoDto.class));
    }

    public Response<ReclamoDto> buscarReclamosPorCodigoCliente(String codigoCliente) {
        var cliente = clienteRepository.findByCodigoCliente(codigoCliente);
        if(cliente.isEmpty()) {
            return new Response<ReclamoDto>().error();
        }
        var Reclamos = mapListaEntidades(cliente.get().getReclamos(), ReclamoDto.class);
        return new Response<ReclamoDto>().ok().data(Reclamos);
    }

    public Response<ReclamoDto> buscarReclamosPorCodigoProducto(String codigoProducto) {
        var reclamos = reclamoRepository.findByProductoAsociado_CodigoProducto(codigoProducto);
        var Reclamos = mapListaEntidades(reclamos, ReclamoDto.class);
        return new Response<ReclamoDto>().ok().data(Reclamos);
    }

    public Response<ReclamoDto> registrarReclamo(ReclamoDto reclamoDto) {
        var clienteOpt = clienteRepository.findByCodigoCliente(reclamoDto.getCodigoCliente());
        if(clienteOpt.isEmpty()) {
            return new Response<ReclamoDto>().error();
        }
        var cliente = clienteOpt.get();
        var producto = getEntityReference(Producto.class, reclamoDto.getCodigoProducto());
        var reclamo = mapDto(reclamoDto, Reclamo.class);
        reclamo.setProductoAsociado(producto);
        cliente.getReclamos().add(reclamo);
        cliente = clienteRepository.save(cliente);
        return new Response<ReclamoDto>().ok().entity(mapEntidad(cliente.getReclamos().get(cliente.getReclamos().size() - 1), ReclamoDto.class));
    }
}
