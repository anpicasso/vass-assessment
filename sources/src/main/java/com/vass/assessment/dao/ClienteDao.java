package com.vass.assessment.dao;

import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.ReclamoDto;
import com.vass.assessment.dto.response.Response;
import com.vass.assessment.model.Cliente;
import com.vass.assessment.model.Producto;
import com.vass.assessment.model.Reclamo;
import com.vass.assessment.repository.ClienteRepository;
import com.vass.assessment.repository.ProductoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import org.modelmapper.Converters;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ClienteDao extends AbstractDao<Cliente, ClienteDto>  {

    @Autowired
    public ClienteDao(ModelMapper modelMapper, ProductoRepository productoRepository, ClienteRepository clienteRepository, ReclamoRepository reclamoRepository) {
        super(modelMapper, productoRepository, clienteRepository, reclamoRepository);
    }

    public Response<ClienteDto> registrarCliente(ClienteDto clienteDto) {
        var cliente = mapDto(clienteDto, Cliente.class);
        cliente = clienteRepository.save(cliente);
        clienteDto.setCodigoCliente(cliente.getCodigoCliente());
        return new Response<ClienteDto>().ok().entity(clienteDto);
    }

    public Response<ClienteDto> listarCliente() {
        var clientes = mapListaEntidades(clienteRepository.findAll(), ClienteDto.class);
        return new Response<ClienteDto>().ok().data(clientes);
    }

    public Response<ClienteDto> buscarCliente(String codigoCliente) {
        var cliente = clienteRepository.findByCodigoCliente(codigoCliente);
        if(cliente.isEmpty()) {
            return new Response<ClienteDto>().error();
        }
        return new Response<ClienteDto>().ok().entity(mapEntidad(cliente.get(), ClienteDto.class));
    }

    public Response<ClienteDto> contratarProducto(ProductoDto productoDto) {
        var clienteOpt = clienteRepository.findByCodigoCliente(productoDto.getCodigoCliente());
        if(clienteOpt.isEmpty()) {
            return new Response<ClienteDto>().error();
        }
        var cliente = clienteOpt.get();
        var producto = getEntityReference(Producto.class, productoDto.getCodigoProducto());
        cliente.getProductos().add(producto);
        cliente = clienteRepository.save(cliente);
        return new Response<ClienteDto>().ok().entity(mapEntidad(cliente, ClienteDto.class));
    }

}
