package com.vass.assessment.dao;

import com.vass.assessment.dto.ClienteDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.ProductoDto;
import com.vass.assessment.dto.response.Response;
import com.vass.assessment.model.Cliente;
import com.vass.assessment.model.Producto;
import com.vass.assessment.repository.ClienteRepository;
import com.vass.assessment.repository.ProductoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductoDao extends AbstractDao<Producto, ProductoDto> {

    @Autowired
    public ProductoDao(ModelMapper modelMapper, ProductoRepository productoRepository, ClienteRepository clienteRepository, ReclamoRepository reclamoRepository) {
        super(modelMapper, productoRepository, clienteRepository, reclamoRepository);
    }

    public Response<ProductoDto> registrarProducto(ProductoDto productoDto) {
        var producto = mapDto(productoDto, Producto.class);
        producto = productoRepository.save(producto);
        productoDto.setCodigoProducto(producto.getCodigoProducto());
        return new Response<ProductoDto>().ok().entity(productoDto);
    }

    public Response<ProductoDto> listarProducto() {
        var productos = mapListaEntidades(productoRepository.findAll(), ProductoDto.class);
        return new Response<ProductoDto>().ok().data(productos);
    }

    public Response<ProductoDto> buscarProducto(String codigoProducto) {
        var producto = productoRepository.findByCodigoProducto(codigoProducto);
        if(producto.isEmpty()) {
            return new Response<ProductoDto>().error();
        }
        return new Response<ProductoDto>().ok().entity(mapEntidad(producto.get(), ProductoDto.class));
    }

    public Response<ProductoDto> buscarProductosPorCodigoCliente(String codigoCliente) {
        var cliente = clienteRepository.findByCodigoCliente(codigoCliente);
        if(cliente.isEmpty()) {
            return new Response<ProductoDto>().error();
        }
        var productos = mapListaEntidades(cliente.get().getProductos(), ProductoDto.class);
        return new Response<ProductoDto>().ok().data(productos);
    }

}
