package com.vass.assessment.dao;

import com.vass.assessment.model.Cliente;
import com.vass.assessment.repository.ClienteRepository;
import com.vass.assessment.repository.ProductoRepository;
import com.vass.assessment.repository.ReclamoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractDao<T,K> {

    @PersistenceContext
    private EntityManager entityManager;

    protected ModelMapper modelMapper;
    protected ProductoRepository productoRepository;
    protected ClienteRepository clienteRepository;
    protected ReclamoRepository reclamoRepository;

    @Autowired
    public AbstractDao(ModelMapper modelMapper, ProductoRepository productoRepository, ClienteRepository clienteRepository, ReclamoRepository reclamoRepository) {
        this.modelMapper = modelMapper;
        this.productoRepository = productoRepository;
        this.clienteRepository = clienteRepository;
        this.reclamoRepository = reclamoRepository;
    }

    protected K mapEntidad(T entidad, Class<K> dtoClazz) {
        return modelMapper.map(entidad, dtoClazz);
    }

    protected T mapDto(K entidad, Class<T> dtoClazz) {
        return modelMapper.map(entidad, dtoClazz);
    }

    protected List<K> mapListaEntidades(List<T> entidades, Class<K> dtoClazz) {
        return entidades.stream().map(entidad -> mapEntidad(entidad, dtoClazz)).collect(Collectors.toList());
    }

    protected <Z> Z getEntityReference(Class<Z> clazz, String codigo) {
        return entityManager.getReference(clazz, codigo);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
