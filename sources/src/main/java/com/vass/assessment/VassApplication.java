package com.vass.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VassApplication {

    public static void main(String[] args) {
        SpringApplication.run(VassApplication.class, args);
    }

}