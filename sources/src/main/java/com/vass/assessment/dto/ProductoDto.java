package com.vass.assessment.dto;

import com.vass.assessment.type.Tecnologia;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class ProductoDto {

    @ApiModelProperty(notes = "Código de entidad. No se usa en los registros, este valor se autogenera, solo se usa en las consultas")
    private String codigoProducto;
    private String nombreProducto;
    private List<Tecnologia> tecnologias;
    @ApiModelProperty(notes = "Código de cliente. Solo se usa en la contratación de un producto.")
    private String codigoCliente;

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public List<Tecnologia> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(List<Tecnologia> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

}
