package com.vass.assessment.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel
public class Response<T> {

    private static final String COD_OK = "00";
    private static final String COD_ERR = "99";

    private String code;
    private List<T> data;
    private T entity;

    public Response() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public Response<T> ok() {
        setCode(COD_OK);
        return this;
    }

    public Response<T> error() {
        setCode(COD_ERR);
        return this;
    }

    public Response<T> code(String code) {
        setCode(code);
        return this;
    }

    public Response<T> data(List<T> data) {
        setData(data);
        return this;
    }

    public Response<T> entity(T entity) {
        setEntity(entity);
        return this;
    }

}
