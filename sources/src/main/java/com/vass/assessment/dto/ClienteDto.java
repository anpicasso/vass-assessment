package com.vass.assessment.dto;

import com.vass.assessment.type.TipoDocumentoCliente;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@ApiModel
public class ClienteDto {

    @ApiModelProperty(notes = "Código de entidad. No se usa en los registros, este valor se autogenera, solo se usa en las consultas")
    private String codigoCliente;
    private TipoDocumentoCliente tipoDocumento;
    private String numeroDocumento;
    private LocalDate fechaNacimiento;

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public TipoDocumentoCliente getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoCliente tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

}
