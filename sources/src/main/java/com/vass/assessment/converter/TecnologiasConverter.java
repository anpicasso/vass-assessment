package com.vass.assessment.converter;

import com.vass.assessment.type.Tecnologia;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class TecnologiasConverter implements AttributeConverter<List<Tecnologia>, String> {

    @Override
    public String convertToDatabaseColumn(List<Tecnologia> tecs) {
        return tecs.toString();
    }

    @Override
    public List<Tecnologia> convertToEntityAttribute(String str) {
        List<Tecnologia> tecs = new ArrayList<>();
        str = str.replace("[", "").replace("]", "").replace(" ", "");
        for(String t : str.split(",")) {
            tecs.add(Tecnologia.valueOf(t));
        }
        return tecs;
    }
}
