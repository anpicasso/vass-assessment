CREATE SCHEMA vass;


ALTER SCHEMA vass OWNER TO postgres;

SET default_tablespace = '';

CREATE TABLE vass.clientes (
    codigo_cliente character varying(255) NOT NULL,
    fecha_nacimiento date,
    numero_documento character varying(255),
    tipo_documento character varying(255)
);


ALTER TABLE vass.clientes OWNER TO postgres;

CREATE TABLE vass.clientes_productos (
    cliente_codigo_cliente character varying(255) NOT NULL,
    productos_codigo_producto character varying(255) NOT NULL
);


ALTER TABLE vass.clientes_productos OWNER TO postgres;

CREATE TABLE vass.clientes_reclamos (
    cliente_codigo_cliente character varying(255) NOT NULL,
    reclamos_producto_asociado_codigo_producto character varying(255) NOT NULL
);


ALTER TABLE vass.clientes_reclamos OWNER TO postgres;

CREATE TABLE vass.productos (
    codigo_producto character varying(255) NOT NULL,
    nombre_producto character varying(255),
    tecnologias character varying(255)
);


ALTER TABLE vass.productos OWNER TO postgres;

CREATE TABLE vass.reclamos (
    descripcion_reclamo character varying(255),
    motivo_reclamo character varying(255),
    producto_asociado_codigo_producto character varying(255) NOT NULL
);


ALTER TABLE vass.reclamos OWNER TO postgres;


ALTER TABLE ONLY vass.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (codigo_cliente);


ALTER TABLE ONLY vass.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (codigo_producto);


ALTER TABLE ONLY vass.reclamos
    ADD CONSTRAINT reclamos_pkey PRIMARY KEY (producto_asociado_codigo_producto);


ALTER TABLE ONLY vass.clientes
    ADD CONSTRAINT uk_2viccgf178bd74vfbq8ctsv8t UNIQUE (numero_documento);


ALTER TABLE ONLY vass.clientes_reclamos
    ADD CONSTRAINT uk_pfhiwc50gp5jcj1xevcpqvt UNIQUE (reclamos_producto_asociado_codigo_producto);


ALTER TABLE ONLY vass.clientes_productos
    ADD CONSTRAINT uk_t3oq5y0e9ec6990pfxlfa7i02 UNIQUE (productos_codigo_producto);


ALTER TABLE ONLY vass.reclamos
    ADD CONSTRAINT fkh3ts6gi7bwrnnjfbrow960i9j FOREIGN KEY (producto_asociado_codigo_producto) REFERENCES vass.productos(codigo_producto);


ALTER TABLE ONLY vass.clientes_productos
    ADD CONSTRAINT fkmgvikw087s28ck8bhwygpsfke FOREIGN KEY (cliente_codigo_cliente) REFERENCES vass.clientes(codigo_cliente);


ALTER TABLE ONLY vass.clientes_reclamos
    ADD CONSTRAINT fkn16vhyerj61aqinw55gf22crk FOREIGN KEY (reclamos_producto_asociado_codigo_producto) REFERENCES vass.reclamos(producto_asociado_codigo_producto);


ALTER TABLE ONLY vass.clientes_productos
    ADD CONSTRAINT fkn95gtcyse47uemihy16ocu1pq FOREIGN KEY (productos_codigo_producto) REFERENCES vass.productos(codigo_producto);


ALTER TABLE ONLY vass.clientes_reclamos
    ADD CONSTRAINT fknkxh1m66dcteyjspcjef0e57e FOREIGN KEY (cliente_codigo_cliente) REFERENCES vass.clientes(codigo_cliente);


