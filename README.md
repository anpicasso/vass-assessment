# VASS ASSESSMENT
## Tecnologias
* OpenJDK 11
* SpringBoot 2.4.3
* PostgreSQL 11.5

## Configurar
Abrir el archivo **sources/src/main/resources/application.yml** y agregar la configuración de BD.
El schema que se ponga en la llave *jpa.properties.hibernate.default-schema* debe existir.

## Compilar
Ejecutar el maven goal *mvn clean install* dentro de la carpeta **/sources**, se creará un archivo *.jar* dentro de **/sources/target/**
El reporte de JaCoCo también se generará dentro de dicha carpeta.
## Ejecutar
Ejecutar ***java -jar target/assesment-0.0.1.jar*** dentro de la carpeta **/sources**.